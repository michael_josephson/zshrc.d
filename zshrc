#-*- mode: sh -*-
#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

[ $TERM = "dumb" ] && unsetopt zle && PS1='$ '

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

export WORKON_HOME=~/.venvs
export PROJECT_HOME=~/projects
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export TERM=xterm-256color

# Emacs
#alias emacs='open -a /Applications/Emacs.app'
#alias e='/usr/local/bin/emacs'
#alias emc='/usr/local/bin/emacsclient -c -n'
#alias em='/usr/local/bin/emacsclient -n'

#   `brew install coreutils`
#if $(gls &>/dev/null)
#then
alias ls="ls -F --color"
alias l="ls -lAh --color"
alias ll="ls -l --color"
alias la='ls -A --color'
#fi

if [[ $OSTYPE == darwin* ]]; then
    file=/opt/local/bin/virtualenvwrapper.sh
    alias vmrun="/Applications/VMware\ Fusion.app/Contents/Library/vmrun"
else
  file=/usr/bin/virtualenvwrapper.sh
fi

if [ -f $file ]; then
  source $file
fi

fpath=(~/.zsh/completion $fpath)
autoload -Uz compinit && compinit -i
